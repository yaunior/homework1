using UnityEngine;

public class Capsule : MonoBehaviour
{
    [SerializeField] private float _speedScale;

    private void Update()
    {
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(_speedScale, _speedScale, _speedScale));
    }
}
