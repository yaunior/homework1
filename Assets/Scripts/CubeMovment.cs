using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovment : MonoBehaviour
{
    [SerializeField] private float _speedMovment;
    [SerializeField] private float _speedRotate;
    [SerializeField] private float _speedScale;
    
    private void Update()
    {
        transform.Translate(_speedMovment * Time.deltaTime * Vector3.forward);
        transform.Rotate(_speedRotate * Time.deltaTime * Vector3.up);
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(_speedScale, _speedScale, _speedScale));
    }
}
